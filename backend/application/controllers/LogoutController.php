<?php

$currentFilePath = dirname(realpath(__FILE__));
set_include_path($currentFilePath . '/../../application/'  . PATH_SEPARATOR . get_include_path());

include 'BaseController.php';

class LogoutController extends BaseController
{

    public function indexAction()
    {
        $session = new Zend_Session_Namespace('LoguedUser');
        $session->user = NULL;
        return $this->_redirect("/index");
    }

}

