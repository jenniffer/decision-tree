<?php

$currentFilePath = dirname(realpath(__FILE__));
set_include_path($currentFilePath . '/../../application/'  . PATH_SEPARATOR . get_include_path());

include 'BaseController.php';

class AdminController extends BaseController
{

    public function indexAction()
    {
        $session = new Zend_Session_Namespace('LoguedUser');
        if($session->user==NULL){
            return $this->_redirect("/login");
        }
        $interviewModel = new Application_Model_Interviews();
        $modelPending = new Application_Model_Pendings();
        $modelUser = new Application_Model_Users();

        $user = $session->user;
        $this->view->user = $user;

        $interviews = $interviewModel->fetchAll()->toArray();
        $this->view->interviews = $interviews;

        $users = $modelUser->fetchAll()->toArray();
        $this->view->users = $users;

        $pendings = $modelPending->getByState(0);
        $this->view->pendings = $pendings;
        
        if($user->type == 0){
            $form = new Application_Form_CreateInterview();
            $this->view->form = $form;

            $form_anl = new Application_Form_RegisterAnl();
            $this->view->form_anl = $form_anl;

            if ( $this->getRequest()->isPost() )
            {
                if ($form->isValid($_POST)) {
                    $bind = array();
                    $bind['title'] = $form->getValue('title');
                    $interviewModel->createInterview($bind);
                }
                if ($form_anl->isValid($_POST)) {
                    $bind = array();
                    $bind['username'] = $form_anl->getValue('username');
                    $bind['password'] = $form_anl->getValue('password');
                    $bind['email'] = $form_anl->getValue('email');
                    $bind['type'] = 1;
                    $modelUser->createUser($bind);
                }
            }
        }
        
    }

}

