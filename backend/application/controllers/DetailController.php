<?php

$currentFilePath = dirname(realpath(__FILE__));
set_include_path($currentFilePath . '/../../application/'  . PATH_SEPARATOR . get_include_path());

include 'BaseController.php';

class DetailController extends BaseController{

    public function indexAction(){

        $session = new Zend_Session_Namespace('InterviewSession');
        $modelInterview = new Application_Model_Interviews();
        $modelQuestion = new Application_Model_Questions();
        $modelResponse = new Application_Model_Responses();
        $modelAnswer = new Application_Model_Answers();
        $modelPending = new Application_Model_Pendings();
        $modelApplicant = new Application_Model_Applicants();
        $modelUser = new Application_Model_Users();

        $id_pending = $this->_getParam('id');
        $this->view->id = $id_pending;
        $id_applicant = $modelPending->getById($id_pending)['id_applicant'];
        
        $questions = $modelResponse->getResponsesByApplicantId($id_applicant);
        $this->view->questions = $questions;

        $users = $modelUser->fetchAll()->toArray();
        $this->view->users = $users;

        if ( $this->getRequest()->isPost() ){
            if($this->_hasParam("Enviar")) {
                $modelApplicant = new Application_Model_Applicants();
                $modelInterview = new Application_Model_Interviews();

                $applicant = $this->getRequest()->getPost();
                $applicant['id'] = $modelApplicant->createApplicant($applicant);

                $session->applicant = $applicant;

                $interviews = $modelInterview->getAll();
                if (count($interviews)>0) {
                    return $this->_redirect("/interview?id=".$interviews[0]['id_interview']);
                }
            }
        }
    }
}

