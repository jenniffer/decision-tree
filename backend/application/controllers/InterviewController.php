<?php

$currentFilePath = dirname(realpath(__FILE__));
set_include_path($currentFilePath . '/../../application/'  . PATH_SEPARATOR . get_include_path());

include 'BaseController.php';

class InterviewController extends BaseController{

    public function indexAction(){
        
        $session = new Zend_Session_Namespace('InterviewSession');
        if (!$this->_hasParam("id") ||
            $session->applicant == NULL ) {
            return $this->_redirect("/");
        }

        if($this->_hasParam("id")) {
            $modelInterview = new Application_Model_Interviews();
            $modelQuestion = new Application_Model_Questions();
            $modelResponse = new Application_Model_Responses();
            $modelAnswer = new Application_Model_Answers();
            $modelPending = new Application_Model_Pendings();
            $modelUser = new Application_Model_Users();

            $id = $this->_getParam("id");
            $interview = $modelInterview->getById($id);
            $questions = $modelQuestion->getByInterview($id);
            $this->view->questions = $questions;

            if ( $this->getRequest()->isPost() ){
                if($this->_hasParam("Enviar")) {

                    $responses = $this->getRequest()->getPost();
                    foreach ($responses as $response_key => $response_value) {
                        if($response_key != 'Enviar'){
                            $bind = array();
                            $bind['id_question'] = $response_key;
                            $bind['id_applicant'] = $session->applicant['id'];
                            $bind['response'] = $response_value;
                            $modelResponse->createResponse($bind);
                        }
                    }

                    $bind = array();
                    $bind['id_interview'] = $id;
                    $bind['id_applicant'] = $session->applicant['id'];

                    $modelAnswer->createAnswer($bind);

                    $interviews = $modelInterview->getAll();
                    foreach ($interviews as $interview_key => $interview_value) {
                        if(!$modelAnswer->isSolved($interview_value['id_interview'], $session->applicant['id'])){
                            return $this->_redirect("/interview?id=".$interview_value['id_interview']);
                        }
                    }
                    $bind = array();
                    $bind['id_interview'] = $id;
                    $bind['id_applicant'] = $session->applicant['id'];
                    $admin = $modelUser->getAdmin()['id_user'];
                    $bind['id_user'] = $admin;
                    $modelPending->createPending($bind);
                    return $this->_redirect("/success");
                    
                }
            }

        }
    }
}
