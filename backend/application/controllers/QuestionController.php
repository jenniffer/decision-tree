<?php

$currentFilePath = dirname(realpath(__FILE__));
set_include_path($currentFilePath . '/../../application/'  . PATH_SEPARATOR . get_include_path());

include 'BaseController.php';

class QuestionController extends BaseController
{

    public function indexAction()
    {
        $session = new Zend_Session_Namespace('LoguedUser');
        if($session->user==NULL){
            return $this->_redirect("/login");
        }
        if(!$this->_hasParam('id')){
            return $this->_redirect("/admin");    
        }
        $interviewModel = new Application_Model_Interviews();
        $questionModel = new Application_Model_Questions();

        $id_interview = $this->_getParam('id');
        $interview = $interviewModel->getById($id_interview);
        $questions = $questionModel->getByInterview($id_interview);

        $this->view->interview = $interview;
        $this->view->questions = $questions;

        $form = new Application_Form_CreateQuestion();
        $this->view->form = $form;

        if ( $this->getRequest()->isPost() )
        {
            if ($form->isValid($_POST)) {
                $bind = array();
                $bind['text'] = $form->getValue('text');
                $bind['id_interview'] = $id_interview;
                $questionModel->createQuestion($bind);
            }
        }
    }

}

