<?php

$currentFilePath = dirname(realpath(__FILE__));
set_include_path($currentFilePath . '/../../application/'  . PATH_SEPARATOR . get_include_path());

include 'BaseController.php';

class IndexController extends BaseController{

    public function indexAction(){

        $session = new Zend_Session_Namespace('InterviewSession');
        $modelInterview = new Application_Model_Interviews();
        $modelQuestion = new Application_Model_Questions();
        $modelResponse = new Application_Model_Responses();
        $modelAnswer = new Application_Model_Answers();

        if($session && $session->applicant['id']){
            $interviews = $modelInterview->getAll();
            foreach ($interviews as $interview_key => $interview_value) {
                if(!$modelAnswer->isSolved($interview_key, $session->applicant['id'])){
                    return $this->_redirect("/interview?id=".$interview_value['id_interview']);
                }
            }
        }

        if ( $this->getRequest()->isPost() ){
            if($this->_hasParam("Enviar")) {
                $modelApplicant = new Application_Model_Applicants();
                $modelInterview = new Application_Model_Interviews();

                $applicant = $this->getRequest()->getPost();
                $applicant['id'] = $modelApplicant->createApplicant($applicant);

                $session->applicant = $applicant;

                $interviews = $modelInterview->getAll();
                if (count($interviews)>0) {
                    return $this->_redirect("/interview?id=".$interviews[0]['id_interview']);
                }
            }
        }
    }
}

