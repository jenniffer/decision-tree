<?php

$currentFilePath = dirname(realpath(__FILE__));
set_include_path($currentFilePath . '/../../application/'  . PATH_SEPARATOR . get_include_path());

include 'BaseController.php';

class ActionController extends BaseController
{

    public function deleteinterviewAction()
    {
        $modelInterview = new Application_Model_Interviews();
        $modelQuestions = new Application_Model_Questions();
        
        $id_interview = $this->_getParam("id");
        
        echo $modelInterview->deleteByID($id_interview);
        echo $modelQuestions->deleteByInterviewID($id_interview);
        
        return $this->_redirect("/admin");
    }

    public function deletequestionAction()
    {
        $modelQuestions = new Application_Model_Questions();
        
        $id_question = $this->_getParam("id");
        echo $modelQuestions->deleteByID($id_question);
        
        return $this->_redirect("/admin");
    }

    public function deleteuserAction()
    {
        $modelQuestions = new Application_Model_Users();
        
        $id_user = $this->_getParam("id");
        echo $modelQuestions->deleteByID($id_user);
        
        return $this->_redirect("/admin");
    }

    public function deletependingAction()
    {
        $modelPendings = new Application_Model_Pendings();
        
        $id_pending = $this->_getParam("id");
        echo $modelPendings->deleteByID($id_pending);
        
        return $this->_redirect("/admin");
    }

    public function assignuserAction()
    {
    	$modelPendings = new Application_Model_Pendings();
        
        $id_pending = $this->_getParam("id");
        $id_new_user = $this->_getParam("to");
        echo $modelPendings->changeIDUser($id_pending, $id_new_user);
        
        return $this->_redirect("/admin");
    }

    public function approveAction()
    {
    	$modelPendings = new Application_Model_Pendings();
        
        $id_pending = $this->_getParam("id");
        $id_new_user = $this->_getParam("to");
        echo $modelPendings->approve($id_pending);
        
        return $this->_redirect("/admin");
    }

    public function discardAction()
    {
    	$modelPendings = new Application_Model_Pendings();
        
        $id_pending = $this->_getParam("id");
        $id_new_user = $this->_getParam("to");
        echo $modelPendings->discard($id_pending);
        
        return $this->_redirect("/admin");
    }

}

