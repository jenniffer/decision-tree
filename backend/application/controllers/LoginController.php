<?php

$currentFilePath = dirname(realpath(__FILE__));
set_include_path($currentFilePath . '/../../application/'  . PATH_SEPARATOR . get_include_path());

include 'BaseController.php';

class LoginController extends BaseController
{

    public function indexAction(){

        $session = new Zend_Session_Namespace('LoguedUser');
        if($session->user!=NULL){
            return $this->_redirect("/admin");
        }
        $db = $this->_getParam('db');
        $form = new Application_Form_Login();
        if ( $this->getRequest()->isPost() )
        {
            if ($form->isValid($_POST)) {
                $db = $this->_getParam('db');
                $users = new Application_Model_Users();                
                $authAdapter = new Zend_Auth_Adapter_DbTable(
                    $db,
                    'users',
                    'username',
                    'password',
                    'MD5(CONCAT(?))'
                    );
                $username=$form->getValue('username');
                $authAdapter->setIdentity($form->getValue('username'));
                $authAdapter->setCredential($form->getValue('password'));
                $auth   = Zend_Auth::getInstance();
                $result = $auth->authenticate($authAdapter);
                if ($result->isValid()) {
                    $session->user = $users->getUser($username);
                    return $this->_redirect("/admin");
                }else{
                    return $this->_redirect("/login?error=true");
                }
            }
        }
        $this->view->form = $form;
    }

}

