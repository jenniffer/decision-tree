<?php
class Application_Form_CreateInterview extends Zend_Form
{
    private $label="Nombre: ";
    
    public function init(){
        $this->addElement(
            'text','title',array(
                'label' => $this->label,
                'required' => 'true'
            )
        );
        
        $this->addElement(
            'submit','Crear Entrevista',array()
        );
        
    }
}