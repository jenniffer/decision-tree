<?php
class Application_Form_CreateQuestion extends Zend_Form
{
    private $label="Texto: ";
    
    public function init(){
        $this->addElement(
            'text','text',array(
                'label' => $this->label,
                'required' => 'true'
            )
        );
        
        $this->addElement(
            'submit','Enviar',array()
        );
        
    }
}