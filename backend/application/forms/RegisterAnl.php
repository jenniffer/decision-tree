<?php
class Application_Form_RegisterAnl extends Zend_Form
{
    private $labelUser="Nombre de usuario: ";
    private $labelPass="Password: ";
    private $labelEmail="Email: ";
    
    public function init(){
        $this->addElement(
            'text','username',array(
                'label' => $this->labelUser,
                'required' => 'true'
            )
        );

        $this->addElement(
            'password','password',array(
                'label' => $this->labelPass,
                'required' => 'true'
            )
        );

        $this->addElement(
            'text','email',array(
                'label' => $this->labelEmail,
                'required' => 'true'
            )
        );
        
        $this->addElement(
            'submit','Crear usuario',array()
        );
        
    }
}