<?php
class Application_Form_Login extends Zend_Form
{
    private $labelUser="Nombre de usuario: ";
    private $labelPass="Password: ";
    
    public function init(){
        $this->addElement(
            'text','username',array(
                'label' => $this->labelUser,
                'required' => 'true'
            )
        );

        $this->addElement(
            'password','password',array(
                'label' => $this->labelPass,
                'required' => 'true'
            )
        );
        
        $this->addElement(
            'submit','Enviar',array()
        );
        
    }
}