<?php
date_default_timezone_set("America/Bogota");
class Application_Model_Pendings extends Zend_Db_Table_Abstract
{
    protected $_name='pendings';
    protected $_primary='id_pending';
	
	public function createPending($bind){
		$row = $this->createRow();
		$row->setFromArray($bind);
		$id = $row->save();
	}
	
	public function getByInterview($id_interview)
	{
		return $this->fetchAll(
								$this->select()	
								->where('id_interview=?', $id_interview )
			)->toArray();
	}

	public function getByState($state)
	{
		return $this->fetchAll(
								$this->select()	
								->where('state=?', $state )
			)->toArray();
	}

	public function getById($id)
	{
		return $this->fetchRow($this->select()->where('id_pending=?',$id)); 
	}

	public function changeIDUser($id_pending, $id_user)
	{
		$row = $this->fetchRow($this->select()->where('id_pending=?', $id_pending));
        if ($row != NULL) {
            $row->id_user = $id_user;
            $row->save();
        }
	}

	public function approve($id_pending)
	{
		$row = $this->fetchRow($this->select()->where('id_pending=?', $id_pending));
        if ($row != NULL) {
            $row->state = 1;
            $row->save();
        }
	}

	public function discard($id_pending)
	{
		$row = $this->fetchRow($this->select()->where('id_pending=?', $id_pending));
        if ($row != NULL) {
            $row->state = 2;
            $row->save();
        }
	}

	public function isSolved($id_interview, $id_applicant){
		$inteviews = $this->fetchAll(
								$this->select()	
								->where('id_interview=?', $id_interview )
								->where('id_applicant=?', $id_applicant )
					)->toArray();
		if(count($inteviews)==0){
			return false;
		}else{
			return true;
		}
	}

	public function deleteByInterviewID($id_interview)
	{
		return $this->delete('id_interview='.$id_interview);
	}

	public function deleteByID( $id )
	{
		$this->delete( 'id_pending=' . $id  );
	}
}