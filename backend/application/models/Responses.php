<?php
date_default_timezone_set("America/Bogota");
class Application_Model_Responses extends Zend_Db_Table_Abstract
{
    protected $_name='responses';
    protected $_primary='id_response';

	public function createResponse($bind){
		$row = $this->createRow();
		$row->setFromArray($bind);
		$id = $row->save();
	}
	
	public function isQuestionAnsweredByApplicant($id_interview, $id_applicant)
	{
		$row = $this->fetchRow( $this	->select()
										->where( 'id_interview=?', $id_interview )
										->where( 'id_applicant=?', $id_applicant ) );
		if (is_null($row))
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	public function getResponsesByApplicantId($id_applicant)
	{
		return $this->fetchAll( $this->
								select()->
								setIntegrityCheck(false)->
								from(
									array('t' => 'interviews'),
									array('id_interview', 'title', 'q.text')
								)
								->join(
									array('q' => 'questions'),'q.id_interview = t.id_interview',
									array('question'=>'q.text') 
								)
								->join(
									array('r' => 'responses'),'q.id_question = r.id_question',
									array('response'=>'r.response') 
								)
								->where('r.id_applicant = ?', $id_applicant)
							)
					->toArray();
	}
	
	public function getAll()
	{
		return $this->fetchAll();
	}

	public function deleteByID( $id )
	{
		$this->delete( 'id_response=' . $id  );
	}
}