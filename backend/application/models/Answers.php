<?php
date_default_timezone_set("America/Bogota");
class Application_Model_Answers extends Zend_Db_Table_Abstract
{
    protected $_name='answers';
    protected $_primary='id_answer';
	
	public function createAnswer($bind){
		$row = $this->createRow();
		$row->setFromArray($bind);
		$id = $row->save();
	}
	
	public function getByInterview($id_interview)
	{
		return $this->fetchAll(
								$this->select()	
								->where('id_interview=?', $id_interview )
			)->toArray();
	}

	public function isSolved($id_interview, $id_applicant){
		$inteviews = $this->fetchAll(
								$this->select()	
								->where('id_interview=?', $id_interview )
								->where('id_applicant=?', $id_applicant )
					)->toArray();
		if(count($inteviews)==0){
			return false;
		}else{
			return true;
		}
	}

	public function deleteByInterviewID($id_interview)
	{
		return $this->delete('id_interview='.$id_interview);
	}
}