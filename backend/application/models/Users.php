<?php
date_default_timezone_set("America/Bogota");
class Application_Model_Users extends Zend_Db_Table_Abstract
{
    protected $_name='users';
    protected $_primary='id_user';
	
    public function createUser( $bind)
    {
        $row_aux = $this->fetchRow($this->select()->where('email=?',$bind['email']));
        if(is_null($row_aux)){
            $row = $this->createRow();
            $row->setFromArray($bind);
            $row->password = md5($bind['password']);
            $row->save();
            return true;
        }
        return false;
    }
	
	public function updateUser($email , $bind )
    {
        $row = $this->fetchRow($this->select()->where('email=?', $email ));
        if($row!=NULL){
            $row->setFromArray($bind);
            $row->save();
        }
    }
	
	public function updatePass($email , $bind )
    {
        $row = $this->fetchRow($this->select()->where('email=?',$email));
        if($row!=NULL){
            $row->password = md5($bind['password']);
            $row->save();
        }
    }
	
	public function getUser($username)
	{
		return $this->fetchRow($this->select()->where('username=?',$username)); 
	}
	
	public function getById($id)
	{
		return $this->fetchRow($this->select()->where('id_user=?',$id)); 
	}
	
	public function getAdmin()
	{
        return $this->fetchRow($this->select()->where('type=?',0)); 
	}
	
	public function changePass($userForm , $password){
		$row_aux = $this->fetchRow($this->select()->where('email=?',$userForm)); 
		$row_aux->password = md5($password);
		$row_aux->save();
	}

    public function deleteByID( $id )
    {
        $this->delete( 'id_user=' . $id  );
    }
}
