<?php
date_default_timezone_set("America/Bogota");
class Application_Model_Questions extends Zend_Db_Table_Abstract
{
    protected $_name='questions';
    protected $_primary='id_question';
	
	public function createQuestion($bind){
		$row = $this->createRow();
		$row->setFromArray($bind);
		$id = $row->save();
	}
	
	public function getByInterview($interview_id)
	{
		return $this->fetchAll(
								$this->select()	
								->where('id_interview=?', $interview_id )
			)->toArray();
	}

	public function deleteByInterviewID($interview_id)
	{
		return $this->delete('id_interview='.$interview_id);
	}

	public function deleteByID($id_question)
	{
		return $this->delete('id_question='.$id_question);
	}
}