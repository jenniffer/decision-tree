<?php

date_default_timezone_set("America/Bogota");

class Application_Model_Applicants extends Zend_Db_Table_Abstract {

    protected $_name = 'applicants';
    protected $_primary = 'id_applicant';

    public function createApplicant($bind) {
        $row = $this->createRow();
        $row->setFromArray($bind);
        $row->created_at = new Zend_Db_Expr('NOW()');
        $id = $row->save();
        return $id;
    }

    public function getById($applicant_id) {
        return $this->fetchRow($this->select()->where('id_applicant=?', $applicant_id));
    }

    public function setApplicant($applicant_id, $result) {
        $row = $this->fetchRow($this->select()->where('applicant=?', $applicant_id));
        if ($row != NULL) {
            $row->applicant_result = $result;
            $row->save();
        } else {
            $row = $this->createRow();
            $row->applicant_result = $result;
            $id = $row->save();
        }
    }

    public function getAll() {
        return $this->fetchAll($this->select()
                                ->order(array('applicant_result DESC', 'created_at ASC'))
                        )
                        ->toArray();
    }

}
