<?php
date_default_timezone_set("America/Bogota");
class Application_Model_Interviews extends Zend_Db_Table_Abstract
{
    protected $_name='interviews';
    protected $_primary='id_interview';
	
	public function createInterview($bind){
		$row = $this->createRow();
		$row->setFromArray($bind);
		return $row->save();
	}

	public function getAll()
	{
		return $this->fetchAll( $this->
								select()->
								setIntegrityCheck(false)->
								from(
									array('t' => 'interviews'),
									array('id_interview', 'title')
								)
								->join(
									array('q' => 'questions'),'q.id_interview = t.id_interview',
									array('question'=>'q.text') 
								)
								->group('t.id_interview')
							)
					->toArray();
	}



	public function getByID( $id )
	{
		return $this->fetchRow( $this->select()->where( 'id_interview=?' , $id ) );
	}
	
	public function deleteByID( $id )
	{
		return $this->delete( 'id_interview='.$id  );
	}

}