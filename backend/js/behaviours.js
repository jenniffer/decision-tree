// JavaScript Document

$( document ).ready(function() {		
						
			//SMOOTH NAVIGATION									
	

				// change behaviour of anchor links to scroll instead of jump
				$(document).on("click", "a[href^=#]", function (e) {
					var
						id = $(this).attr("href"),
						$elem = $(id);
					if ($elem.length > 0) {
						e.preventDefault();
						TweenMax.to(window, 2, {scrollTo: {y: $elem.offset().top}});
						if (window.history && window.history.pushState) {
							// if supported by the browser we can even update the URL.
							history.pushState("", document.title, id);
						}
					}
				});

/*ABOUT THE PROGRAM SELECTOR FUNCTIONS*/				
$("#cat1_program").click(function(){
		if($("#content-qa1_program").hasClass("selected")){
		$("#content-qa1_program").slideToggle("slow");
		$("#cat1_program .arrow-cat").slideToggle("fast");
		$("#cat1_program .arrow-cat").removeClass("selected");
		$("#content-qa1_program").removeClass("selected");
		$("#cat1_program").removeClass("cat-qa-selected");
		//console.log("content tiene selected")
		
	}else if(!($("#content-qa1_program").hasClass("selected"))){
		$(".wrapper-content-qa .clearfix").find(".selected").slideUp().removeClass("selected");
		$(".wrapper-content-qa .cat-qa").find(".selected").slideUp().removeClass("selected");
		$(".wrapper-content-qa").find("div.cat-qa-selected").removeClass("cat-qa-selected");
		$("#content-qa1_program").slideToggle("slow");
		$("#content-qa1_program").addClass("selected");
		$("#cat1_program .arrow-cat").slideToggle("fast");
		$("#cat1_program .arrow-cat").addClass("selected");
		$("#cat1_program").addClass("cat-qa-selected");
		//console.log("===content NO tiene selected")
		}
		
	})


$("#cat2_program").click(function(){
	if($("#content-qa2_program").hasClass("selected")){
		$("#content-qa2_program").slideToggle("slow");
		$("#cat2_program .arrow-cat").slideToggle("fast");
		$("#cat2_program .arrow-cat").removeClass("selected");
		$("#content-qa2_program").removeClass("selected");
		$("#cat2_program").removeClass("cat-qa-selected");
		//console.log("content tiene selected")
		
	}else if(!($("#content-qa2_program").hasClass("selected"))){
		$(".wrapper-content-qa .clearfix").find(".selected").slideUp().removeClass("selected");
		$(".wrapper-content-qa .cat-qa").find(".selected").slideUp().removeClass("selected");
		$(".wrapper-content-qa").find("div.cat-qa-selected").removeClass("cat-qa-selected");
		$("#content-qa2_program").slideToggle("slow");
		$("#content-qa2_program").addClass("selected");
		$("#cat2_program .arrow-cat").slideToggle("fast");
		$("#cat2_program .arrow-cat").addClass("selected");
		$("#cat2_program").addClass("cat-qa-selected");
		//console.log("===content NO tiene selected")
		}
	})


$("#cat3_program").click(function(){
	if($("#content-qa3_program").hasClass("selected")){
		$("#content-qa3_program").slideToggle("slow");
		$("#cat3_program .arrow-cat").slideToggle("fast");
		$("#cat3_program .arrow-cat").removeClass("selected");
		$("#content-qa3_program").removeClass("selected");
		$("#cat3_program").removeClass("cat-qa-selected");
		//console.log("content tiene selected")
		
	}else if(!($("#content-qa3_program").hasClass("selected"))){
		$(".wrapper-content-qa .clearfix").find(".selected").slideUp().removeClass("selected");
		$(".wrapper-content-qa .cat-qa").find(".selected").slideUp().removeClass("selected");
		$(".wrapper-content-qa").find("div.cat-qa-selected").removeClass("cat-qa-selected");
		$("#content-qa3_program").slideToggle("slow");
		$("#content-qa3_program").addClass("selected");
		$("#cat3_program .arrow-cat").slideToggle("fast");
		$("#cat3_program .arrow-cat").addClass("selected");
		$("#cat3_program").addClass("cat-qa-selected");
		//console.log("===content NO tiene selected")
		}
	})


$("#cat4_program").click(function(){
	if($("#content-qa4_program").hasClass("selected")){
		$("#content-qa4_program").slideToggle("slow");
		$("#cat4_program .arrow-cat").slideToggle("fast");
		$("#cat4_program .arrow-cat").removeClass("selected");
		$("#content-qa4_program").removeClass("selected");
		$("#cat4_program").removeClass("cat-qa-selected");
		//console.log("content tiene selected")
		
	}else if(!($("#content-qa4_program").hasClass("selected"))){
		$(".wrapper-content-qa .clearfix").find(".selected").slideUp().removeClass("selected");
		$(".wrapper-content-qa .cat-qa").find(".selected").slideUp().removeClass("selected");
		$(".wrapper-content-qa").find("div.cat-qa-selected").removeClass("cat-qa-selected");
		$("#content-qa4_program").slideToggle("slow");
		$("#content-qa4_program").addClass("selected");
		$("#cat4_program .arrow-cat").slideToggle("fast");
		$("#cat4_program .arrow-cat").addClass("selected");
		$("#cat4_program").addClass("cat-qa-selected");
		//console.log("===content NO tiene selected")
		}
	})


$("#cat5_program").click(function(){
	if($("#content-qa5_program").hasClass("selected")){
		$("#content-qa5_program").slideToggle("slow");
		$("#cat5_program .arrow-cat").slideToggle("fast");
		$("#cat5_program .arrow-cat").removeClass("selected");
		$("#content-qa5_program").removeClass("selected");
		$("#cat5_program").removeClass("cat-qa-selected");
		//console.log("content tiene selected")
		
	}else if(!($("#content-qa5_program").hasClass("selected"))){
		$(".wrapper-content-qa .clearfix").find(".selected").slideUp().removeClass("selected");
		$(".wrapper-content-qa .cat-qa").find(".selected").slideUp().removeClass("selected");
		$(".wrapper-content-qa").find("div.cat-qa-selected").removeClass("cat-qa-selected");
		$("#content-qa5_program").slideToggle("slow");
		$("#content-qa5_program").addClass("selected");
		$("#cat5_program .arrow-cat").slideToggle("fast");
		$("#cat5_program .arrow-cat").addClass("selected");
		$("#cat5_program").addClass("cat-qa-selected");
		//console.log("===content NO tiene selected")
		}
	})


$("#cat6_program").click(function(){
	if($("#content-qa6_program").hasClass("selected")){
		$("#content-qa6_program").slideToggle("slow");
		$("#cat6_program .arrow-cat").slideToggle("fast");
		$("#cat6_program .arrow-cat").removeClass("selected");
		$("#content-qa6_program").removeClass("selected");
		$("#cat6_program").removeClass("cat-qa-selected");
		//console.log("content tiene selected")
		
	}else if(!($("#content-qa6_program").hasClass("selected"))){
		$(".wrapper-content-qa .clearfix").find(".selected").slideUp().removeClass("selected");
		$(".wrapper-content-qa .cat-qa").find(".selected").slideUp().removeClass("selected");
		$(".wrapper-content-qa").find("div.cat-qa-selected").removeClass("cat-qa-selected");
		$("#content-qa6_program").slideToggle("slow");
		$("#content-qa6_program").addClass("selected");
		$("#cat6_program .arrow-cat").slideToggle("fast");
		$("#cat6_program .arrow-cat").addClass("selected");
		$("#cat6_program").addClass("cat-qa-selected");
		//console.log("===content NO tiene selected")
		}
	})



$("#cat7_program").click(function(){
	if($("#content-qa7_program").hasClass("selected")){
		$("#content-qa7_program").slideToggle("slow");
		$("#cat7_program .arrow-cat").slideToggle("fast");
		$("#cat7_program .arrow-cat").removeClass("selected");
		$("#content-qa7_program").removeClass("selected");
		$("#cat7_program").removeClass("cat-qa-selected");
		//console.log("content tiene selected")
		
	}else if(!($("#content-qa7_program").hasClass("selected"))){
		$(".wrapper-content-qa .clearfix").find(".selected").slideUp().removeClass("selected");
		$(".wrapper-content-qa .cat-qa").find(".selected").slideUp().removeClass("selected");
		$(".wrapper-content-qa").find("div.cat-qa-selected").removeClass("cat-qa-selected");
		$("#content-qa7_program").slideToggle("slow");
		$("#content-qa7_program").addClass("selected");
		$("#cat7_program .arrow-cat").slideToggle("fast");
		$("#cat7_program .arrow-cat").addClass("selected");
		$("#cat7_program").addClass("cat-qa-selected");
		//console.log("===content NO tiene selected")
		}
	})
	
	
	
$("#cat8_program").click(function(){
	if($("#content-qa8_program").hasClass("selected")){
		$("#content-qa8_program").slideToggle("slow");
		$("#cat8_program .arrow-cat").slideToggle("fast");
		$("#cat8_program .arrow-cat").removeClass("selected");
		$("#content-qa8_program").removeClass("selected");
		$("#cat8_program").removeClass("cat-qa-selected");
		//console.log("content tiene selected")
		
	}else if(!($("#content-qa8_program").hasClass("selected"))){
		$(".wrapper-content-qa .clearfix").find(".selected").slideUp().removeClass("selected");
		$(".wrapper-content-qa .cat-qa").find(".selected").slideUp().removeClass("selected");
		$(".wrapper-content-qa").find("div.cat-qa-selected").removeClass("cat-qa-selected");
		$("#content-qa8_program").slideToggle("slow");
		$("#content-qa8_program").addClass("selected");
		$("#cat8_program .arrow-cat").slideToggle("fast");
		$("#cat8_program .arrow-cat").addClass("selected");
		$("#cat8_program").addClass("cat-qa-selected");
		//console.log("===content NO tiene selected")
		}
	})


$("#cat9_program").click(function(){
	if($("#content-qa9_program").hasClass("selected")){
		$("#content-qa9_program").slideToggle("slow");
		$("#cat9_program .arrow-cat").slideToggle("fast");
		$("#cat9_program .arrow-cat").removeClass("selected");
		$("#content-qa9_program").removeClass("selected");
		$("#cat9_program").removeClass("cat-qa-selected");
		//console.log("content tiene selected")
		
	}else if(!($("#content-qa9_program").hasClass("selected"))){
		$(".wrapper-content-qa .clearfix").find(".selected").slideUp().removeClass("selected");
		$(".wrapper-content-qa .cat-qa").find(".selected").slideUp().removeClass("selected");
		$(".wrapper-content-qa").find("div.cat-qa-selected").removeClass("cat-qa-selected");
		$("#content-qa9_program").slideToggle("slow");
		$("#content-qa9_program").addClass("selected");
		$("#cat9_program .arrow-cat").slideToggle("fast");
		$("#cat9_program .arrow-cat").addClass("selected");
		$("#cat9_program").addClass("cat-qa-selected");
		//console.log("===content NO tiene selected")
		}
	})
	
	
	
$("#cat10_program").click(function(){
	if($("#content-qa10_program").hasClass("selected")){
		$("#content-qa10_program").slideToggle("slow");
		$("#cat10_program .arrow-cat").slideToggle("fast");
		$("#cat10_program .arrow-cat").removeClass("selected");
		$("#content-qa10_program").removeClass("selected");
		$("#cat10_program").removeClass("cat-qa-selected");
		//console.log("content tiene selected")
		
	}else if(!($("#content-qa10_program").hasClass("selected"))){
		$(".wrapper-content-qa .clearfix").find(".selected").slideUp().removeClass("selected");
		$(".wrapper-content-qa .cat-qa").find(".selected").slideUp().removeClass("selected");
		$(".wrapper-content-qa").find("div.cat-qa-selected").removeClass("cat-qa-selected");
		$("#content-qa10_program").slideToggle("slow");
		$("#content-qa10_program").addClass("selected");
		$("#cat10_program .arrow-cat").slideToggle("fast");
		$("#cat10_program .arrow-cat").addClass("selected");
		$("#cat10_program").addClass("cat-qa-selected");
		//console.log("===content NO tiene selected")
		}
	})
	
	
		
	
$("#btn-close-1-program").click(function(){

		$("#content-qa1_program").slideToggle("slow");
		$("#cat1_program .arrow-cat").slideToggle("fast");
		$("#cat1_program .arrow-cat").removeClass("selected");
		$("#content-qa1_program").removeClass("selected");
		$("#cat1_program").removeClass("cat-qa-selected");
		//console.log("content tiene selected")
		

	})
	
$("#btn-close-2-program").click(function(){

		$("#content-qa2_program").slideToggle("slow");
		$("#cat2_program .arrow-cat").slideToggle("fast");
		$("#cat2_program .arrow-cat").removeClass("selected");
		$("#content-qa2_program").removeClass("selected");
		$("#cat2_program").removeClass("cat-qa-selected");
		//console.log("content tiene selected")
		

	})	
	
$("#btn-close-3-program").click(function(){

		$("#content-qa3_program").slideToggle("slow");
		$("#cat3_program .arrow-cat").slideToggle("fast");
		$("#cat3_program .arrow-cat").removeClass("selected");
		$("#content-qa3_program").removeClass("selected");
		$("#cat3_program").removeClass("cat-qa-selected");
		//console.log("content tiene selected")
		

	})	
	
$("#btn-close-4-program").click(function(){

		$("#content-qa4_program").slideToggle("slow");
		$("#cat4_program .arrow-cat").slideToggle("fast");
		$("#cat4_program .arrow-cat").removeClass("selected");
		$("#content-qa4_program").removeClass("selected");
		$("#cat4_program").removeClass("cat-qa-selected");
		//console.log("content tiene selected")
		
	})		
	
$("#btn-close-5-program").click(function(){

		$("#content-qa5_program").slideToggle("slow");
		$("#cat5_program .arrow-cat").slideToggle("fast");
		$("#cat5_program .arrow-cat").removeClass("selected");
		$("#content-qa5_program").removeClass("selected");
		$("#cat5_program").removeClass("cat-qa-selected");
		//console.log("content tiene selected")
		
	})	
	
	$("#btn-close-6-program").click(function(){

		$("#content-qa6_program").slideToggle("slow");
		$("#cat6_program .arrow-cat").slideToggle("fast");
		$("#cat6_program .arrow-cat").removeClass("selected");
		$("#content-qa6_program").removeClass("selected");
		$("#cat6_program").removeClass("cat-qa-selected");
		//console.log("content tiene selected")
		
	})		
	
	
	$("#btn-close-7-program").click(function(){

		$("#content-qa7_program").slideToggle("slow");
		$("#cat7_program .arrow-cat").slideToggle("fast");
		$("#cat7_program .arrow-cat").removeClass("selected");
		$("#content-qa7_program").removeClass("selected");
		$("#cat7_program").removeClass("cat-qa-selected");
		//console.log("content tiene selected")
		
	})
	
	$("#btn-close-8-program").click(function(){

		$("#content-qa8_program").slideToggle("slow");
		$("#cat8_program .arrow-cat").slideToggle("fast");
		$("#cat8_program .arrow-cat").removeClass("selected");
		$("#content-qa8_program").removeClass("selected");
		$("#cat8_program").removeClass("cat-qa-selected");
		//console.log("content tiene selected")
		
	})	
	
	$("#btn-close-9-program").click(function(){

		$("#content-qa9_program").slideToggle("slow");
		$("#cat9_program .arrow-cat").slideToggle("fast");
		$("#cat9_program .arrow-cat").removeClass("selected");
		$("#content-qa9_program").removeClass("selected");
		$("#cat9_program").removeClass("cat-qa-selected");
		//console.log("content tiene selected")
		
	})	
	
	
	$("#btn-close-10-program").click(function(){

		$("#content-qa10_program").slideToggle("slow");
		$("#cat10_program .arrow-cat").slideToggle("fast");
		$("#cat10_program .arrow-cat").removeClass("selected");
		$("#content-qa10_program").removeClass("selected");
		$("#cat10_program").removeClass("cat-qa-selected");
		//console.log("content tiene selected")
		
	})		
	
/*ABOUT MILEAGEPLUS SELECTOR FUNCTIONS*/	
	
$("#cat1_milleageplus").click(function(){
		if($("#content-qa1_milleageplus").hasClass("selected")){
		$("#content-qa1_milleageplus").slideToggle("slow");
		$("#cat1_milleageplus .arrow-cat").slideToggle("fast");
		$("#cat1_milleageplus .arrow-cat").removeClass("selected");
		$("#content-qa1_milleageplus").removeClass("selected");
		$("#cat1_milleageplus").removeClass("cat-qa-selected");
		//console.log("content tiene selected")
		
	}else if(!($("#content-qa1_milleageplus").hasClass("selected"))){
		$(".wrapper-content-qa .clearfix").find(".selected").slideUp().removeClass("selected");
		$(".wrapper-content-qa .cat-qa").find(".selected").slideUp().removeClass("selected");
		$(".wrapper-content-qa").find("div.cat-qa-selected").removeClass("cat-qa-selected");
		$("#content-qa1_milleageplus").slideToggle("slow");
		$("#content-qa1_milleageplus").addClass("selected");
		$("#cat1_milleageplus .arrow-cat").slideToggle("fast");
		$("#cat1_milleageplus .arrow-cat").addClass("selected");
		$("#cat1_milleageplus").addClass("cat-qa-selected");
		//console.log("===content NO tiene selected")
		}
		
	})


$("#cat2_milleageplus").click(function(){
	if($("#content-qa2_milleageplus").hasClass("selected")){
		$("#content-qa2_milleageplus").slideToggle("slow");
		$("#cat2_milleageplus .arrow-cat").slideToggle("fast");
		$("#cat2_milleageplus .arrow-cat").removeClass("selected");
		$("#content-qa2_milleageplus").removeClass("selected");
		$("#cat2_milleageplus").removeClass("cat-qa-selected");
		//console.log("content tiene selected")
		
	}else if(!($("#content-qa2_milleageplus").hasClass("selected"))){
		$(".wrapper-content-qa .clearfix").find(".selected").slideUp().removeClass("selected");
		$(".wrapper-content-qa .cat-qa").find(".selected").slideUp().removeClass("selected");
		$(".wrapper-content-qa").find("div.cat-qa-selected").removeClass("cat-qa-selected");
		$("#content-qa2_milleageplus").slideToggle("slow");
		$("#content-qa2_milleageplus").addClass("selected");
		$("#cat2_milleageplus .arrow-cat").slideToggle("fast");
		$("#cat2_milleageplus .arrow-cat").addClass("selected");
		$("#cat2_milleageplus").addClass("cat-qa-selected");
		//console.log("===content NO tiene selected")
		}
	})


$("#cat3_milleageplus").click(function(){
	if($("#content-qa3_milleageplus").hasClass("selected")){
		$("#content-qa3_milleageplus").slideToggle("slow");
		$("#cat3_milleageplus .arrow-cat").slideToggle("fast");
		$("#cat3_milleageplus .arrow-cat").removeClass("selected");
		$("#content-qa3_milleageplus").removeClass("selected");
		$("#cat3_milleageplus").removeClass("cat-qa-selected");
		//console.log("content tiene selected")
		
	}else if(!($("#content-qa3_milleageplus").hasClass("selected"))){
		$(".wrapper-content-qa .clearfix").find(".selected").slideUp().removeClass("selected");
		$(".wrapper-content-qa .cat-qa").find(".selected").slideUp().removeClass("selected");
		$(".wrapper-content-qa").find("div.cat-qa-selected").removeClass("cat-qa-selected");
		$("#content-qa3_milleageplus").slideToggle("slow");
		$("#content-qa3_milleageplus").addClass("selected");
		$("#cat3_milleageplus .arrow-cat").slideToggle("fast");
		$("#cat3_milleageplus .arrow-cat").addClass("selected");
		$("#cat3_milleageplus").addClass("cat-qa-selected");
		//console.log("===content NO tiene selected")
		}
	})


$("#cat4_milleageplus").click(function(){
	if($("#content-qa4_milleageplus").hasClass("selected")){
		$("#content-qa4_milleageplus").slideToggle("slow");
		$("#cat4_milleageplus .arrow-cat").slideToggle("fast");
		$("#cat4_milleageplus .arrow-cat").removeClass("selected");
		$("#content-qa4_milleageplus").removeClass("selected");
		$("#cat4_milleageplus").removeClass("cat-qa-selected");
		//console.log("content tiene selected")
		
	}else if(!($("#content-qa4_milleageplus").hasClass("selected"))){
		$(".wrapper-content-qa .clearfix").find(".selected").slideUp().removeClass("selected");
		$(".wrapper-content-qa .cat-qa").find(".selected").slideUp().removeClass("selected");
		$(".wrapper-content-qa").find("div.cat-qa-selected").removeClass("cat-qa-selected");
		$("#content-qa4_milleageplus").slideToggle("slow");
		$("#content-qa4_milleageplus").addClass("selected");
		$("#cat4_milleageplus .arrow-cat").slideToggle("fast");
		$("#cat4_milleageplus .arrow-cat").addClass("selected");
		$("#cat4_milleageplus").addClass("cat-qa-selected");
		//console.log("===content NO tiene selected")
		}
	})


$("#cat5_milleageplus").click(function(){
	if($("#content-qa5_milleageplus").hasClass("selected")){
		$("#content-qa5_milleageplus").slideToggle("slow");
		$("#cat5_milleageplus .arrow-cat").slideToggle("fast");
		$("#cat5_milleageplus .arrow-cat").removeClass("selected");
		$("#content-qa5_milleageplus").removeClass("selected");
		$("#cat5_milleageplus").removeClass("cat-qa-selected");
		//console.log("content tiene selected")
		
	}else if(!($("#content-qa5_milleageplus").hasClass("selected"))){
		$(".wrapper-content-qa .clearfix").find(".selected").slideUp().removeClass("selected");
		$(".wrapper-content-qa .cat-qa").find(".selected").slideUp().removeClass("selected");
		$(".wrapper-content-qa").find("div.cat-qa-selected").removeClass("cat-qa-selected");
		$("#content-qa5_milleageplus").slideToggle("slow");
		$("#content-qa5_milleageplus").addClass("selected");
		$("#cat5_milleageplus .arrow-cat").slideToggle("fast");
		$("#cat5_milleageplus .arrow-cat").addClass("selected");
		$("#cat5_milleageplus").addClass("cat-qa-selected");
		//console.log("===content NO tiene selected")
		}
	})


$("#cat6_milleageplus").click(function(){
	if($("#content-qa6_milleageplus").hasClass("selected")){
		$("#content-qa6_milleageplus").slideToggle("slow");
		$("#cat6_milleageplus .arrow-cat").slideToggle("fast");
		$("#cat6_milleageplus .arrow-cat").removeClass("selected");
		$("#content-qa6_milleageplus").removeClass("selected");
		$("#cat6_milleageplus").removeClass("cat-qa-selected");
		//console.log("content tiene selected")
		
	}else if(!($("#content-qa6_milleageplus").hasClass("selected"))){
		$(".wrapper-content-qa .clearfix").find(".selected").slideUp().removeClass("selected");
		$(".wrapper-content-qa .cat-qa").find(".selected").slideUp().removeClass("selected");
		$(".wrapper-content-qa").find("div.cat-qa-selected").removeClass("cat-qa-selected");
		$("#content-qa6_milleageplus").slideToggle("slow");
		$("#content-qa6_milleageplus").addClass("selected");
		$("#cat6_milleageplus .arrow-cat").slideToggle("fast");
		$("#cat6_milleageplus .arrow-cat").addClass("selected");
		$("#cat1_progra6").addClass("cat-qa-selected");
		//console.log("===content NO tiene selected")
		}
	})
		
		


$("#btn-close-1-milleageplus").click(function(){

		$("#content-qa1_milleageplus").slideToggle("slow");
		$("#cat1_milleageplus .arrow-cat").slideToggle("fast");
		$("#cat1_milleageplus .arrow-cat").removeClass("selected");
		$("#content-qa1_milleageplus").removeClass("selected");
		$("#cat1_milleageplus").removeClass("cat-qa-selected");
		//console.log("content tiene selected")
		

	})
	
$("#btn-close-2-milleageplus").click(function(){

		$("#content-qa2_milleageplus").slideToggle("slow");
		$("#cat2_milleageplus .arrow-cat").slideToggle("fast");
		$("#cat2_milleageplus .arrow-cat").removeClass("selected");
		$("#content-qa2_milleageplus").removeClass("selected");
		$("#cat2_milleageplus").removeClass("cat-qa-selected");
		//console.log("content tiene selected")
		

	})	
	
$("#btn-close-3-milleageplus").click(function(){

		$("#content-qa3_milleageplus").slideToggle("slow");
		$("#cat3_milleageplus .arrow-cat").slideToggle("fast");
		$("#cat3_milleageplus .arrow-cat").removeClass("selected");
		$("#content-qa3_milleageplus").removeClass("selected");
		$("#cat3_milleageplus").removeClass("cat-qa-selected");
		//console.log("content tiene selected")
		

	})	
	
$("#btn-close-4-milleageplus").click(function(){

		$("#content-qa4_milleageplus").slideToggle("slow");
		$("#cat4_milleageplus .arrow-cat").slideToggle("fast");
		$("#cat4_milleageplus .arrow-cat").removeClass("selected");
		$("#content-qa4_milleageplus").removeClass("selected");
		$("#cat4_milleageplus").removeClass("cat-qa-selected");
		//console.log("content tiene selected")
		
	})		
	
$("#btn-close-5-milleageplus").click(function(){

		$("#content-qa5_milleageplus").slideToggle("slow");
		$("#cat5_milleageplus .arrow-cat").slideToggle("fast");
		$("#cat5_milleageplus .arrow-cat").removeClass("selected");
		$("#content-qa5_milleageplus").removeClass("selected");
		$("#ca51_milleageplus").removeClass("cat-qa-selected");
		//console.log("content tiene selected")
		
	})	
	
	$("#btn-close-6-milleageplus").click(function(){

		$("#content-qa6_milleageplus").slideToggle("slow");
		$("#cat6_milleageplus .arrow-cat").slideToggle("fast");
		$("#cat6_milleageplus .arrow-cat").removeClass("selected");
		$("#content-qa6_milleageplus").removeClass("selected");
		$("#cat6_milleageplus").removeClass("cat-qa-selected");
		//console.log("content tiene selected")
		
	})		
	
		
});
