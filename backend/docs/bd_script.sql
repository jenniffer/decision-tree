
CREATE TABLE IF NOT EXISTS `Interviews` (
  `id_interview` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(500) NOT NULL,
  PRIMARY KEY (`id_interview`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

CREATE TABLE IF NOT EXISTS `Questions` (
  `id_question` int(11) NOT NULL AUTO_INCREMENT,
  `id_interview` int(11) NOT NULL,
  `text` varchar(500) NOT NULL,
  PRIMARY KEY (`id_question`),
  KEY `question_interview_ibfk` (`id_interview`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

CREATE TABLE IF NOT EXISTS `Applicants` (
  `id_applicant` int(11) NOT NULL AUTO_INCREMENT,
  `applicant_name` varchar(50) NOT NULL,
  `applicant_lastname` varchar(50) NOT NULL,
  `applicant_identification` varchar(50) NOT NULL,
  `applicant_phone` varchar(50) NOT NULL,
  `applicant_email` varchar(50) NOT NULL,
  `applicant_last_job_name` varchar(50) NOT NULL,
  `applicant_last_job_charge` varchar(50) NOT NULL,
  `applicant_last_job_boss` varchar(50) NOT NULL,
  `applicant_last_job_phone` varchar(50) NOT NULL,
  `applicant_personal_reference_name` varchar(50) NOT NULL,
  `applicant_personal_reference_lasname` varchar(50) NOT NULL,
  `applicant_personal_reference_phone` varchar(50) NOT NULL,
  `applicant_work_reference_name` varchar(50) NOT NULL,
  `applicant_work_reference_lasname` varchar(50) NOT NULL,
  `applicant_work_reference_phone` varchar(50) NOT NULL,
  `applicant_school_name` varchar(50) NOT NULL,
  `applicant_school_title` varchar(50) NOT NULL,
  `applicant_university_name` varchar(50) NOT NULL,
  `applicant_university_title` varchar(50) NOT NULL,
  `applicant_result` varchar(50) NOT NULL,
  `created_at` DATE NOT NULL,
  PRIMARY KEY (`id_applicant`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

CREATE TABLE IF NOT EXISTS `Pendings` (
  `id_pending` int(11) NOT NULL AUTO_INCREMENT,
  `id_applicant` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY (`id_pending`),
  KEY `pending_applicant_ibfk` (`id_applicant`),
  KEY `pending_user_ibfk` (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

CREATE TABLE IF NOT EXISTS `Answers` (
  `id_answer` int(11) NOT NULL AUTO_INCREMENT,
  `id_applicant` int(11) NOT NULL,
  `id_interview` int(11) NOT NULL,
  PRIMARY KEY (`id_answer`),
  KEY `answer_applicant_ibfk` (`id_applicant`),
  KEY `answer_interview_ibfk` (`id_interview`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

CREATE TABLE IF NOT EXISTS `Responses` (
  `id_response` int(11) NOT NULL AUTO_INCREMENT,
  `id_question` int(11) NOT NULL,
  `response` varchar(50) NOT NULL,
  `id_applicant` int(11) NOT NULL,
  PRIMARY KEY (`id_response`),
  KEY `response_question_ibfk` (`id_question`),
  KEY `response_applicant_ibfk` (`id_applicant`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

CREATE TABLE IF NOT EXISTS `Users` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(32) DEFAULT NULL,
  `last_login` datetime NOT NULL,
  `type` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

ALTER TABLE `Questions`
  ADD CONSTRAINT `question_interview_ibfk` FOREIGN KEY (`id_interview`) REFERENCES `Interviews` (`id_interview`) ON DELETE CASCADE;

ALTER TABLE `Pending`
  ADD CONSTRAINT `pending_applicant_ibfk` FOREIGN KEY (`id_applicant`) REFERENCES `Applicants` (`id_applicant`) ON DELETE CASCADE,
  ADD CONSTRAINT `pending_user_ibfk` FOREIGN KEY (`id_user`) REFERENCES `Users` (`id_user`) ON DELETE CASCADE;

ALTER TABLE `Answers`
  ADD CONSTRAINT `answer_applicant_ibfk` FOREIGN KEY (`id_applicant`) REFERENCES `Applicants` (`id_applicant`) ON DELETE CASCADE,
  ADD CONSTRAINT `answer_interview_ibfk` FOREIGN KEY (`id_interview`) REFERENCES `Interviews` (`id_interview`) ON DELETE CASCADE;

ALTER TABLE `Responses`
  ADD CONSTRAINT `response_question_ibfk` FOREIGN KEY (`id_question`) REFERENCES `Questions` (`id_question`) ON DELETE CASCADE,
  ADD CONSTRAINT `response_applicant_ibfk` FOREIGN KEY (`id_applicant`) REFERENCES `Applicants` (`id_applicant`) ON DELETE CASCADE;

ALTER TABLE `Pendings` ADD `state` tinyint(3) unsigned NOT NULL DEFAULT '0';